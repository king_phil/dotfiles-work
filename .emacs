(require 'package)
(add-to-list 'package-archives
             ;; '("melpa" . "https://melpa.org/packages/") t)
	     '("melpa-stable" . "https://stable.melpa.org/packages/") t)

(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize) ;; You might already have this line

;; M-x package-install ----> e.g. groovy-mode

;(add-to-list 'load-path (expand-file-name "~/.emacs.d/lisp"))

(setq backup-inhibited t)

;; By an unknown contributor (now slightly modified by PEK)
;; (Changed (interactive "p") to "P", got rid of the ability
;;   to insert multiple %'s at once (never used it...))
(global-set-key "%" 'pek-match-paren)
(defun pek-match-paren (arg)
  "Display matching paren (unless not on a paren or prefix-arg)."
  (interactive "P")
  (cond (arg (self-insert-command 1))
        ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
        ((looking-at "\\s\)") (forward-char 1) (backward-list 1))
        (t (self-insert-command 1))))

;; original concept stolen from Robert Boone
(defun perltidy ()
 "execute perltidy for the selected region or the current buffer"
 (interactive)
 (save-excursion
   (unless mark-active
     (mark-whole-buffer)))
 (shell-command-on-region (point) (mark) "perltidy -q" nil t)
 ;; TODO Check for perltidy.ERR
 (princ "Tidied" t))
(global-set-key "\C-ct" 'perltidy)

(setq initial-scratch-buffer nil)
(find-file (concat (getenv "HOME") "/" "scratch"))
(kill-buffer "*scratch*")

(global-set-key "\M-g" 'goto-line)

;; https://github.com/dominikh/go-mode.el
;(add-to-list 'load-path (concat (getenv "HOME") "/local/src/go-mode.el/"))
;(require 'go-mode-autoloads)

;; http://stackoverflow.com/questions/6172054/how-can-i-random-sort-lines-in-a-buffer
;;? http://www.emacswiki.org/emacs/RandomizeBuffer
;;? https://lists.gnu.org/archive/html/gnu-emacs-sources/2005-04/msg00010.html
(defun my-random-sort-lines (beg end)
  "Sort lines in region randomly."
  (interactive "r")
  (save-excursion
    (save-restriction
      (narrow-to-region beg end)
      (goto-char (point-min))
      (let ;; To make `end-of-line' and etc. to ignore fields.
          ((inhibit-field-text-motion t))
        (sort-subr nil 'forward-line 'end-of-line nil nil
                   (lambda (s1 s2) (eq (random 2) 0)))))))

;; emacs itself adds these when used
(put 'narrow-to-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; pek: holy fuck, emacs; terrible design decision enabling this
(electric-indent-mode -1)

;; pek: t on indent-tabs-mode means "yes, you may input tabs"
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default c-basic-offset 4)

;; Stolen from the python.org emacs site
(add-to-list 'interpreter-mode-alist '("python" . python-mode))
(autoload 'python-mode "python-mode" "Python editing mode." t)
(autoload 'python "python" "Python editing mode." t)
(add-hook 'python-mode-hook
          (lambda ()
            (define-key python-mode-map (kbd "ESC C-u") 'python-beginning-of-block)
            (define-key python-mode-map (kbd "ESC C-d") 'python-end-of-block)))
;; note: using C-M-u to go up to 'def' and 'class', and C-M-(up|down)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CPERL MODE
;; omg, files.el has a clobbery perl-mode auto-mode-alist
(defalias 'perl-mode 'cperl-mode)

(setq cperl-indent-level 4)
(add-to-list 'auto-mode-alist '("\\.t$" . cperl-mode))
(setq cperl-invalid-face nil)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (groovy-mode s))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
