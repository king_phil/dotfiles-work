alias _readConfig="grep -Ev '(^\s*#)|(^\s*$)'"
alias _dashboard="systemctl list-unit-files -t service --state enabled"
alias pekscreen="screen -e '^zz' -R -d -S pek"
alias v6='ping6 yahoo.com'
alias _peklog='PEKLOG=/tmp/_peklog; echo > $PEKLOG; chmod 777 $PEKLOG; tail -f $PEKLOG'

alias myreview='git diff $(git merge-base $(cat .branched_from ) HEAD)'
alias myproject='emacs $(myreview --name-only)'
alias mycatchup='git fetch upstream && git log -p upstream/$(cat .branched_from) ^$(cat .branched_from) --reverse'
alias mysandbox="time make clean-generated-files sandbox 2>&1 | tee ~/f7u12"

export EDITOR=$(which emacs)

PS1='\n\u@\h \w\n\$ '
CP_GIT_PROMPT=/usr/local/cpanel/3rdparty/share/git/contrib/completion/git-prompt.sh
if [ -f $CP_GIT_PROMPT ]; then
    . $CP_GIT_PROMPT
    PS1='\n\u@\h \w$(__git_ps1 " (%s)")\n\$ '
fi

export SANDBOX_LOCALES='en es'
